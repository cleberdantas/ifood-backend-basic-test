package com.ifood.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import com.ifood.models.WeatherInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import com.ifood.services.WeatherService;

@RestController
@RequestMapping(value="/weather")
public class Weather {

	@Autowired
	private WeatherService weatherService;

    @RequestMapping(value="",method = RequestMethod.GET)
    @Cacheable("weatherData")
    public WeatherInfo Weather(@RequestParam(value="city", defaultValue = "campinas") String city) {

		return weatherService.getWeatherInformation(city);
	}
}
