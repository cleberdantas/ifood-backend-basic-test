package com.ifood.services;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.beans.factory.annotation.Value;
import com.ifood.models.WeatherInfo;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class WeatherService {

  private final RestTemplate restTemplate;

  @Value("${weather.api.key}")
  private String apiKey;

  @Value("${weather.api.uri}")
  private String apiUri;

  public WeatherService(RestTemplate rest) {
    this.restTemplate = rest;
  }

  @HystrixCommand(fallbackMethod = "fallback")
  public WeatherInfo getWeatherInformation(String city) {
    UriComponentsBuilder builder = UriComponentsBuilder
      .fromUriString(apiUri)
      .queryParam("appid", apiKey)
      .queryParam("q", city);

    RestTemplate restTemplate = new RestTemplate();
    WeatherInfo info = restTemplate.getForObject(builder.toUriString(), WeatherInfo.class);

    return info;
  }

  public WeatherInfo fallback(String city) {

    WeatherInfo info = new WeatherInfo(city);

    return info;
  }
}