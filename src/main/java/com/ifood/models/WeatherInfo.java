package com.ifood.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;
import java.util.Collection;
import java.io.Serializable;
 
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherInfo implements Serializable {

	private String name;

	@JsonProperty("city")
	public String getName() {
	    return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
	    this.name = name;
	}

	public WeatherInfo(){

	}

	public WeatherInfo(String name){
		this.name = name;
	}

    @JsonProperty(value="weather")
    private Collection<Map<String,String>> weather;

    @JsonProperty(value="main")
    private Map<String,String> main;
}