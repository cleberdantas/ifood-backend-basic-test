package com.ifood.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/about")
public class About {

    @RequestMapping(value="",method = RequestMethod.GET)
    public String about(){
		return "service is up and running";
    }
}
